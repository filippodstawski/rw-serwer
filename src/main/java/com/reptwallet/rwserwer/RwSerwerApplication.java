package com.reptwallet.rwserwer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class RwSerwerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RwSerwerApplication.class, args);
	}
}
